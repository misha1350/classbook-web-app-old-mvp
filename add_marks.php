<?php
include('./query.php');

?>
<!doctype html>
<html>
<meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="./style.css">

	<head>
		<title> Выставление оценок</title>
	</head>
	
	 <form action="" method="post">
			<h5>С помощью этой формы можно добавить в таблицу классного журнала оценку за контрольное мероприятие.</h5>
			<input type="text" name="add_marks" hidden value=
			"INSERT INTO classbook (student_id, event_id, mark, time_of_mark) VALUES">
		
		<p>Выберите мероприятие:
		<select name="event_id">
			<option value="1">Выберите мероприятие</option>
			<? for ($i = 0; $i < count($events); $i++) {?>
				<option value="<?echo $events[$i][0]?>"> <?echo $events[$i][1] . ' ' . $events[$i][2]?> </option>
			<? } ?>
		</select>
		
		<table border="1">
			<thead>
                <tr>
                    <th>Имя</th>
                    <th>Оценка</th>
                </tr>
            </thead>
			<tbody>
		<? for ($i = 0; $i < count($students); $i++) { ?>
			<tr>
				<td><? echo $students[$i][1] ?></td>
				<td>
				<select name="grade[<?echo($i)?>]">
					<option value="-1">Оценка</option>
					<? if ($max_grade[0][3] > 2 and $max_grade[0][3] < 6) {
						for ($j = 2; $j <= ($max_grade[0][3]); $j++) {
						?>
						<option value="<? echo $j?>"> <?echo($j)?> </option>
						<? } ?>
				</select>
				<input type="text" hidden value="<?echo $students[$i][0]?>" name="students[<?echo($i)?>]" value="">
					<? } print_r ($i+1); ?>
				</td>
			</tr>
		<? } ?>
			</tbody>
		</table>
			
		<p>	<input type="submit" value="Проставить оценки"/>
		<button class="button close-button">Отмена</button>
		
		<input type="text" hidden value="<?echo $_POST['subject_name']?>" name="subject_name" value="" required>
		<input type="text" hidden value="<?echo $_POST['class_id']?>" name="class_id" value="" required>
		<input type="text" hidden value="<?echo $db_user?>" name="db_user" size="30" value="" required>
		<input type="password" hidden value="<?echo $user_pw?>" name="user_pw" size="30" value="" />
		</form>
			
		</p>

</html>
