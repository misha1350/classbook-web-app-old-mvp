<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
<?php

/*try {
    $pdo = new PDO('mysql:host=localhost;dbname=new_db', 'student2', '1234');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    print "Error!: " . $e->getMessage() . "<br/>";
    die();
}
*/

// Opens a connection to the database
// Since it is a php file it won't open in a browser 
// It should be saved outside of the main web documents folder
// and imported when needed

/*
Command that gives the database user the least amount of power
as is needed.
GRANT INSERT, SELECT, DELETE, UPDATE ON test3.* 
TO 'studentweb'@'localhost' 
IDENTIFIED BY 'turtledove';
SELECT : Select rows in tables
INSERT : Insert new rows into tables
UPDATE : Change data in rows
DELETE : Delete existing rows (Remove privilege if not required)
*/

/*// Defined as constants so that they can't be changed
DEFINE ('DB_USER', 'student2');
DEFINE ('DB_PASSWORD', '1234');
DEFINE ('DB_HOST', 'localhost');
DEFINE ('DB_NAME', 'new_db');

// $dbc will contain a resource link to the database
// @ keeps the error from showing in the browser

$dbc = @mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME)
OR die('Could not connect to MySQL: ' .
mysqli_connect_error());
*/

$db_host = 'localhost';  //  hostname
$db_name = 'control_activities';  //  databasename
if(!isset($_POST['db_user'])) {
	$db_user = 'student'; 
} else {
	$db_user = $_POST['db_user'];
}

if(!isset($_POST['user_pw'])) {
		$user_pw = '';  // default user's password (there isn't a set password)
} else {
	$user_pw = $_POST['user_pw'];
}

try {
    $pdo = new PDO('mysql:host='.$db_host.';dbname='.$db_name, $db_user, $user_pw);
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
 catch (PDOException $e) {
    print "Error!: " . $e->getMessage() . "<br/>";
	?><b><p style ="color:red">Проверьте данные входа</p></b>
	<p>Логин:
<input type="text" name="db_user" size="30" value="" />
</p>

<p>Пароль:
<input type="password" name="user_pw" size="30" value="" />
</p>

<p>
<input type="submit" value="Войти">
</p>

</form>
</body>
</html>
    <?die();
}

?>

<b><p style="color:green">Logged in as <?=$db_user?></p></b>

<form action="" method="post">

<? if ($db_user == 'student') {
	?>
Войти под другим пользователем:

<p>Логин:
<input type="text" name="db_user" size="30" value="" required>
</p>

Пароль:
<input type="password" name="user_pw" size="30" value="" />
</p>

<p>
<input type="submit" value="Войти">
</p>
<? } ?>
</form>
</body>
</html>