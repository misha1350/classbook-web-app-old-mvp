<?php
//require_once("config.php");


/*

--- Мусорка ---

                    <td>
                        <form action="result.php" method="post">
                            <input type="text" name="sql" hidden value="DELETE FROM <?=$table?>">
                            <button type="submit">Очистить таблицу</button>
                        </form>
                    </td>
                    <td>
                        <form action="result.php" method="post">
                            <input type="text" name="sql" hidden value="DROP TABLE <?=$table?>">
                            <button type="submit">Удалить таблицу</button>
                        </form>
                    </td>
					
					<link rel="stylesheet" href="css/main.css">'
					
			<?php foreach ($eventTypes as $type): ?>
			<?print_r($type[1])?>
			<option value="<?$type[1]?>> <?$type?> </option>
			<?php endforeach; ?>

*/

require_once('./query.php');
?>

<!doctype html>
<html>
<meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="./style.css">

	
	<head>
		<title> Контрольные мероприятия </title>
	</head>
	
	
<? if ($db_user == 'classbook_admin') { ?>
<button class="button open-button">Назначить контрольное мероприятие</button>

<dialog class="modal" id="modal">
  <h2>Назначение контрольного мероприятия</h2>
 <form action="result.php" method="post">
			<h5>Вы можете добавить планируемое или уже проведённое контрольное мероприятие в список мероприятий.</h5>
			<input type="text" name="add_event" hidden value=
			"INSERT INTO eventlist (type_id, subject_name, date, var_amount, var_cost)
			VALUES">
		
		<p>Название предмета:
		<input type="text" name="subject_name" size="30" value="" required />
		</p>
		
		<p>Тип мероприятия:
		<select name="type_name">
			<option value="1">Выберите тип мероприятия</option>
			<? for ($i = 0; $i < count($eventTypes); $i++) {
				?>
				<option value="<?echo $eventTypes[$i][0]?>"> <?echo($eventTypes[$i][1])?> </option>
			<? } ?>
		</select>
		</p>
		
		<p>Дата проведения:
		<input type="datetime-local" name="event_date" required />
		</p>
		
		<p>Количество вариантов:
		<input type="number" name="var_amount" title="Количество вариантов" placeholder="Введите целое число" required />
		</p>
		
		<p>Стоимость варианта:
		 <input type="number" placeholder="0.00" required name="var_cost" min="0" value="0" step="0.0001" 
		 title="Стоимость варианта" pattern="^\d+(?:\.\d{13,4})?$" onblur="
this.parentNode.parentNode.style.backgroundColor=/^\d+(?:\.\d{1,2})?$/.test(this.value)?'inherit':'red'">
		</p>
		
		<p>	<input type="submit" value="Добавить мероприятие"/>
		<button class="button close-button">Отмена</button>
		
		<input type="text" hidden value="<?echo $db_user?>" name="db_user" size="30" value="" required>
		<input type="password" hidden value="<?echo $user_pw?>" name="user_pw" size="30" value="" />
		</form>
			
		</p>

</dialog>

	<? unset($type);
	} ?>
<? /*
<button class="myBtn" id="myBtn1">Open Modal</button>
<dialog class="modal1" id="modal2">
<div class="modal-content">
  <h2>Назначение контрольного мероприятия 2</h2>
 <form action="result.php" method="post">
			<h5>Вы можете добавить планируемое или уже проведённое контрольное мероприятие в список мероприятий.</h5>
			<input type="text" name="add_event" hidden value=
			"INSERT INTO eventlist (type_id, subject_name, date, var_amount, var_cost)
			VALUES">
		
		<p>Название предмета:
		<input type="text" name="subject_name" size="30" value="" required />
		</p>
		
		<p>Тип мероприятия:
		<select name="type_name">
			<option value="1">Выберите тип мероприятия</option>
			<? for ($i = 0; $i < count($eventTypes); $i++) {
				?>
				<option value="<?echo $eventTypes[$i][0]?>"> <?echo($eventTypes[$i][1])?> </option>
			<? } ?>
		</select>
		</p>
		
		<p>Дата проведения:
		<input type="datetime-local" name="event_date" required />
		</p>
		
		<p>Количество вариантов:
		<input type="number" name="var_amount" title="Количество вариантов" placeholder="Введите целое число" required />
		</p>
		
		<p>Стоимость варианта:
		 <input type="number" placeholder="0.00" required name="var_cost" min="0" value="0" step="0.0001" 
		 title="Стоимость варианта" pattern="^\d+(?:\.\d{13,4})?$" onblur="
this.parentNode.parentNode.style.backgroundColor=/^\d+(?:\.\d{1,2})?$/.test(this.value)?'inherit':'red'">
		</p>
		
		<p>	<input type="submit" value="Добавить мероприятие"/>
		<button class="button close-button">Отмена</button>
		
		<input type="text" hidden value="<?echo $db_user?>" name="db_user" size="30" value="" required>
		<input type="password" hidden value="<?echo $user_pw?>" name="user_pw" size="30" value="" />
		</form>
			
		</p>
		</div>
</dialog>
 */?>
 
<? if ($db_user == 'teacher' or $db_user == 'classbook_admin') { ?>
	<form action="add_marks.php" method="post">
		<input type="text" name="search_event" hidden value=
			"SELECT id, subject_name, date FROM eventlist WHERE type_id =">
		
		<p>Класс:
		<select name="class_id">
			<option value="1">Выберите класс</option>
			<? for ($i = 0; $i < count($classes); $i++) {
				?>
				<option value="<?echo $classes[$i][0]?>"> <?echo($classes[$i][1])?> </option>
			<? } ?>
		</select>
		
		<select name="type_name">
			<option value="1">Выберите тип мероприятия</option>
			<? for ($i = 0; $i < count($eventTypes); $i++) {
				?>
				<option value="<?echo $eventTypes[$i][0]?>"> <?echo($eventTypes[$i][1])?> </option>
			<? } ?>
		</select>
		<button type="submit">Выставление оценок</button>
		
		</p>
		
		<input type="text" hidden value="<?echo $db_user?>" name="db_user" size="30" value="" required>
		<input type="password" hidden value="<?echo $user_pw?>" name="user_pw" size="30" value="" />
        </form>
</center>
<? /*
<dialog class="modal" id="modal3">
  <h2>Выставление оценок</h2>
 <form action="result.php" method="post">
			<h5>С помощью этой формы можно добавить в таблицу классного журнала оценку за контрольное мероприятие.</h5>
			<input type="text" name="add_marks" hidden value=
			"INSERT INTO classbook (student_id, event_id, mark, time_of_mark)
			VALUES">
		
		<p>Sample:
		<input type="text" name="" size="30" value="" required />
		</p>
		

		
		<p>Дата проведения:
		<input type="datetime-local" name="event_date" required />
		</p>
		
		<p>Количество вариантов:
		<input type="number" name="var_amount" title="Количество вариантов" placeholder="Введите целое число" required />
		</p>
		
		<p>Стоимость варианта:
		 <input type="number" placeholder="0.00" required name="var_cost" min="0" value="0" step="0.0001" 
		 title="Стоимость варианта" pattern="^\d+(?:\.\d{13,4})?$" onblur="
this.parentNode.parentNode.style.backgroundColor=/^\d+(?:\.\d{1,2})?$/.test(this.value)?'inherit':'red'">
		</p>
		
		<p>	<input type="submit" value="Добавить мероприятие"/>
		<button class="button close-button">Отмена</button>
		
		<input type="text" hidden value="<?echo $db_user?>" name="db_user" size="30" value="" required>
		<input type="password" hidden value="<?echo $user_pw?>" name="user_pw" size="30" value="" />
		</form>
			
		</p>

</dialog>
*/ ?>

<script  src="./script.js"></script>


	<? unset($type);
	} ?>
	
<? if ($db_user == 'classbook_admin' or $db_user == 'teacher') { ?>
		<center>
		<form action="result.php" method="post">
		<textarea name="sql" placeholder="Ваш запрос" cols="40" rows="5" required></textarea>
		<input type="submit" value="Вывести данные">
		</form>
		</center>
<? } ?>
	</div>
	
	
	<center>
    <div style="margin-top: 50px">
        <table border="1">
            <thead>
                <tr>
                    <th>Представление</th>
                    <th>Действия</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($tableList as $table): ?>
                <tr>
                    <td><?=$table?></td>
                    <td>
                        <form action="result.php" method="post">
                            <input type="text" name="sql" hidden value="SELECT * FROM <?=$table?>">
							<input type="text" name="where" placeholder="where ... like ...">
                            <button style="background-color:#333333;color:#00FF00;border-radius:5px" 
							type="submit">Вывести данные</button>
                        </form>
                    </td>

                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
		<form action="report.php" method="post">
		<input type="submit" value="Вывести отчёт">
		</form>
	</center>

</html>
