<?php
require_once('./config.php');
if(isset($_POST['sql'])) {
    $sqlQuery = $_POST['sql'] . ' ' . $_POST['where'];
    try {
        $data = SQL_pdo($pdo, $sqlQuery);
        $messageSuccess = 'Запрос успешно отправлен';
    } catch (PDOException $e) {
        $messageError = $e->getMessage();
    }
}

if(isset($_POST['add_event'])) {
	$unix_timestamp = strtotime($_POST['event_date']);
	gmdate("Y-m-d\TH:i:s\Z", $unix_timestamp);
    $sqlQuery2 = $_POST['add_event'] . ' ' . "(:n1, :n2, :n3, :n4, :n5)";
    try {
        $stmt = $pdo->prepare($sqlQuery2);
		$stmt->execute(array(
		':n1' => $_POST['type_name'],
		':n2' => $_POST['subject_name'],
		':n3' => gmdate("Y-m-d H:i:s", $unix_timestamp),
		':n4' => $_POST['var_amount'],
		':n5' => $_POST['var_cost']));
        $messageSuccess = 'Запрос успешно отправлен';
    } catch (PDOException $e) {
        $messageError = $e->getMessage();
    }
}
/*
if(isset($_POST['add_marks'])) {
	$unix_timestamp = strtotime($_POST['event_date']);
	gmdate("Y-m-d\TH:i:s\Z", $unix_timestamp);
    $sqlQuery2 = $_POST['add_event'] . ' ' . "(:n1, :n2, :n3, :n4, :n5)";
    try {
        $stmt = $pdo->prepare($sqlQuery2);
		$stmt->execute(array(
		':n1' => $_POST['type_name'],
		':n2' => $_POST['subject_name'],
		':n3' => gmdate("Y-m-d H:i:s", $unix_timestamp),
		':n4' => $_POST['var_amount'],
		':n5' => $_POST['var_cost']));
        $messageSuccess = 'Запрос успешно отправлен';
    } catch (PDOException $e) {
        $messageError = $e->getMessage();
    }
}
*/

$events = array();
if(isset($_POST['search_event'])) {
	$sqlQuery3 = $_POST['search_event'] . ' ' . $_POST['type_name'];
	$result3 = $pdo->query($sqlQuery3);
	while ($row = $result3->fetch(PDO::FETCH_NUM)) {
		$events[] = $row;
	}
}


$tableList = array();
$result = $pdo->query("SHOW FULL TABLES WHERE TABLE_TYPE LIKE 'VIEW'");
while ($row = $result->fetch(PDO::FETCH_BOTH)) {
    $tableList[] = $row[0];
}

if ($db_user == 'classbook_admin' or $db_user == 'teacher') {
$eventTypes = array();
$result2 = $pdo->query("SELECT * FROM eventtype");
while ($row = $result2->fetch(PDO::FETCH_NUM)) {
    $eventTypes[] = $row;
}

$classes = array();
$classes_res = $pdo->query("SELECT * FROM class");
while ($row = $classes_res->fetch(PDO::FETCH_NUM)) {
    $classes[] = $row;
	}
}


if(isset($_POST['search_event']) or isset($_POST['add_marks'])) {	
$students = array();
$students_res = $pdo->query("SELECT * FROM students WHERE class_id = " . ' ' . $_POST['class_id']);
while ($row = $students_res->fetch(PDO::FETCH_NUM)) {
    $students[] = $row;
	}
}

if(isset($_POST['search_event'])) {
$max_grade = array();
$max_grade_res = $pdo->query("SELECT * FROM eventtype WHERE id = " . ' ' . $_POST['type_name']);
while ($row = $max_grade_res->fetch(PDO::FETCH_NUM)) {
    $max_grade[] = $row;
	}
}

/*
if(isset($_POST['add_marks']) and isset($_POST['grade']))
	$unix_timestamp = strtotime(now);
	gmdate("Y-m-d\TH:i:s\Z", $unix_timestamp);
	$sqlQuery4 = $_POST['add_marks'] . ' ' . "(:n1, :n2, :n3, :n4)";
	$stmt = $pdo->prepare($sqlQuery4);
    try {
		$pdo->beginTransaction();
		$stmt->execute(array(
		':n1' => $_POST['students'],
		':n2' => $_POST['subject_name'],
		':n3' => $_POST['grade'],
		':n4' => '2022-06-29 12:30:00'//gmdate("Y-m-d H:i:s", $unix_timestamp)
		));
		$pdo->commit();
        $messageSuccess = 'Запрос на мультивставку успешно отправлен';
    } catch (Exception $e) {
        $messageError = $e->getMessage();
		$pdo->rollback();
		throw $e;
    }
?>
*/
<pre> <?
print_r($_POST['add_marks']);
print_r($_POST['grade']); 
print_r($_POST['students']);?>
</pre>
<?
function SQL_pdo($pdo, $Query)
{
    $args = func_get_args ();
    if(count($args) == 1) {
        $result = $pdo->query($Query);

        $type = substr ( trim ( strtoupper ( $Query ) ), 0, 6 );
        if($type == "INSERT") {
            if($result === false)
                return null;

            $res = $pdo->lastInsertId();
            if ($res == 0)
                return array(
                    'type' => $type,
                    'data' => $result->rowCount()
                );

            return array(
                'type' => $type,
                'data' => $res
            );
        }
        elseif ($type == "DELETE" or $type == "UPDATE" or $type == "REPLAC")
            return array(
                'type' => $type,
                'data' => $result->rowCount()
            );

        elseif ($type == "SELECT") {
            $res = $result->fetchAll(PDO::FETCH_ASSOC);
            if ($res === array())
                return array(
                    'type' => $type,
                    'data' => null,
                    'fields' => null
                );

            $fields = array_keys($res[0]);
            return array(
                'type' => $type,
                'data' => $res,
                'fields' => $fields
            );
        }
    } else {
        if (!$stmt = $pdo->prepare($Query))
            return false;

        array_shift($args);
        $i = 0;
        foreach ($args as &$v)
            $stmt->bindValue(++$i, $v);

        $success = $stmt->execute();
        $type = substr(trim(strtoupper($Query)), 0, 6);
        if ($type == "INSERT") {
            if (!$success)
                return null;

            $res = $pdo->lastInsertId();
            if ($res == 0)
                return array(
                    'type' => $type,
                    'data' => $stmt->rowCount()
                );

            return array(
                'type' => $type,
                'data' => $res
            );
        } elseif ($type == "DELETE" or $type == "UPDATE" or $type == "REPLAC") {
            return array(
                'type' => $type,
                'data' => $stmt->rowCount()
            );
        } elseif ($type == "SELECT") {
            $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if($res === array())
                return array(
                    'type' => $type,
                    'data' => null,
                    'fields' => null
                );

            $fields = array_keys($res[0]);
            return array(
                'type' => $type,
                'data' => $res,
                'fields' => $fields
            );
        }
    }
}

?>