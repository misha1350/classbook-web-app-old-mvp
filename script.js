const modal = document.querySelectorAll(".modal");
const openModal = document.querySelectorAll(".open-button");
const closeModal = document.querySelectorAll(".close-button");

openModal[0].addEventListener("click", () => {
  modal[0].showModal();
});

closeModal[0].addEventListener("click", () => {
  modal[0].close();
});

openModal[1].addEventListener("click", () => {
  modal[1].showModal();
});

closeModal[1].addEventListener("click", () => {
  modal[1].close();
});





// Dead code below

// Get the modal
var modal1 = document.getElementsByClassName('modal1');

// Get the button that opens the modal
var btn = document.getElementsByClassName("myBtn");


// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close");

// When the user clicks the button, open the modal 
btn[0].onclick = function() {
    modal1[0].style.display = "block";
}

btn[1].onclick = function() {
    modal1[1].style.display = "block";
}
// When the user clicks on <span> (x), close the modal
span[0].onclick = function() {
    modal1[0].style.display = "none";
}

span[1].onclick = function() {
    modal1[1].style.display = "none";
}
// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal1) {
        modal1.style.display = "none";
    }
}