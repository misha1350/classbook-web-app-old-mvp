# Classbook Web App

A web app for keeping track of the marks of the students of a school, university or other educational institution.

## Features

- Supports adding marks for various kinds of tests, finals, exams, colloquiums, with support for different grading (passed/not passed, 100-point scales, 21-point scales, you name it), as well as your own type of test that you can add yourself via a query that you can send to the database itself (designed to be used by a database admin with minimal knowledge of MySQL)
- Can list results of:
    - tests
    - tests after re-testing
    - tests with regard to the importance of the test (a simple 5-minute test or Gaokao)
    - individual students
    - average grades per test
    - average grades per class
    - average grade per student
- Keeps track of all tests and re-testing attempts by letting users see the dates of the attempts
- Lets users add the price of testing materials for each test, as well as the amount of test variants, and the prices of said test variants
- Shows reports like the total amount of money spent on all testing materials, which can expanded to all kinds of reports as well as forecasts
- BCNF database consisting of only 5 tables, and driven by a bunch of rather clever SQL queries, if I do say so myself
- SQL injection-proof (fairly easy to do actually)


## To-do

- This app could definitely benefit from getting Dockerized
- If it was to go into production, it would need some good GUI (I'm just a backend developer)
- Some reports for better analysis and forecasts
- Some of the PHP load should be transferred to the client side
- Add a couple more modal windows for streamlining the search queries and making them flexible
- Dead code cleaning (hiding and downplaying code crimes)

## More information

This web app was written in PHP with a tiny and healthy amount of JavaScript sprinkled (copy-pasted) in, was designed to work with a self-made MySQL database and was tested in the OpenServer web programming environment. As such, **the app comes with absolutely no guarantees that it will work on your machine without the required configuration.** Feel free to take issue with it (list an issue and I might just do something about it)
