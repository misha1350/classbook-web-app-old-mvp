<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

<style type="text/css">body{margin:40px
auto;max-width:650px;line-height:1.6;font-size:18px;color:#444;padding:0
10px}h1,h2,h3{line-height:1.2}</style>
<center>
	<form action="index.php" method="get">
		<input type="submit" value="Вернуться обратно">
	</form>
</center>

<?php

require_once('./config.php');

$query = "SELECT * FROM employees";

$response = mysqli_query($dbc, $query);

// If the query executed properly proceed
if($response){

echo '<table align="left"
cellspacing="5" cellpadding="8">

<tr><td align="left"><b>First Name</b></td>
<td align="left"><b>Last Name</b></td>
<td align="left"><b>Middle Name</b></td>
<td align="left"><b>Phone Number</b></td>
<td align="left"><b>City</b></td>
<td align="left"><b>Address</b></td>
<td align="left"><b>Salary</b></td>
<td align="left"><b>Months of experience</b></td></tr>';

// mysqli_fetch_array will return a row of data from the query
// until no further data is available
while($row = mysqli_fetch_array($response)){

echo '<tr><td align="left">' . 
$row['first_name'] . '</td><td align="left">' . 
$row['last_name'] . '</td><td align="left">' .
$row['middle_name'] . '</td><td align="left">' . 
$row['phone_number'] . '</td><td align="left">' .
$row['address1'] . '</td><td align="left">' . 
$row['adress2'] . '</td><td align="left">' .
$row['salary'] . '</td><td align="left">' . 
$row['months'] . '</td><td align="left">' .
$row['birth_date'] . '</td><td align="left">';

echo '</tr>';
}

echo '</table>';

} else {

echo "Couldn't issue database query<br />";

echo mysqli_error($dbc);

}

// Close connection to the database
mysqli_close($dbc);

?>